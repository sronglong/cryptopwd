//
//  ColorSchema.swift
//  CryptoPassword
//
//  Created by Chhem Sronglong on 15/12/2017.
//  Copyright © 2017 SureSdey. All rights reserved.
//

import UIKit


class ColorSchema {
    
    // MARK: - Properties
    
    private static var sharedColorManager: ColorSchema = {
        let colorManager = ColorSchema()
        
        // Configuration
        // ...
        
        return colorManager
    }()
    
    // MARK: -
    
    var backgroupColor = UIColor()
    var textColor = UIColor()
    var randomColor1 = UIColor()
    var randomColor2 = UIColor()
    var randomColor3 = UIColor()
    
    // Initialization
    
    init() {
//        self.baseURL = baseURL
        self.backgroupColor = UIColor(red: 249.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1.0)
        self.textColor = UIColor(red: 90.0/255.0, green: 83.0/255.0, blue: 83.0/255.0, alpha: 1.0)
        self.randomColor1 = UIColor(red: 160.0/255.0, green: 113.0/255.0, blue: 120.0/255.0, alpha: 1.0)
        self.randomColor2 = UIColor(red: 144.0/255.0, green: 149.0/255.0, blue: 128.0/255.0, alpha: 1.0)
        self.randomColor3 = UIColor(red: 152.0/255.0, green: 139.0/255.0, blue: 142.0/255.0, alpha: 1.0)
        print("created")
    }
    
    // MARK: - Accessors
    
    class func shared() -> ColorSchema {
        return sharedColorManager
    }
    
    func printMe() {
        print(self.textColor.description);
    }
}

