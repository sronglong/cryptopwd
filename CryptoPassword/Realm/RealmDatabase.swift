//
//  RealmDatabase.swift
//  CryptoPassword
//
//  Created by Chhem Sronglong on 15/12/2017.
//  Copyright © 2017 SureSdey. All rights reserved.
//

import RealmSwift

class RealmDatabase: Object {

    @objc dynamic var tableId = ""
    @objc dynamic var regDate = Date()
    @objc dynamic var serviceName = ""
    @objc dynamic var loginId = ""
    @objc dynamic var loginPassword = ""
    @objc dynamic var authenticationCode: String? = nil
    @objc dynamic var authenticationBaseImage: Data? = nil
    @objc dynamic var securityQuestion: String? = nil
    
}
