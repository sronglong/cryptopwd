//
//  SplashScreenViewController.swift
//  CryptoPassword
//
//  Created by Chhem Sronglong on 15/12/2017.
//  Copyright © 2017 SureSdey. All rights reserved.
//

import UIKit
import CocoaLumberjack

class SplashScreenViewController: UIViewController {
    
    
    class func createInstance() -> SplashScreenViewController {
        let storyboard : UIStoryboard = UIStoryboard(name: "View", bundle: nil)
        let vc : SplashScreenViewController = storyboard.instantiateViewController(withIdentifier: "SplashScreenViewController") as! SplashScreenViewController
        return vc;
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        print("my first swift project")
        ColorSchema.shared().printMe();
        // Do any additional setup after loading the view.
        
        
        DDLog.add(DDTTYLogger.sharedInstance) // TTY = Xcode console
        DDLog.add(DDASLLogger.sharedInstance) // ASL = Apple System Logs
        
        let fileLogger: DDFileLogger = DDFileLogger() // File Logger
        fileLogger.rollingFrequency = TimeInterval(60*60*24)  // 24 hours
        fileLogger.logFileManager.maximumNumberOfLogFiles = 7
        DDLog.add(fileLogger)
            
//            ...
        
        DDLogVerbose("Verbose");
        DDLogDebug("Debug");
        DDLogInfo("Info");
        DDLogWarn("Warn");
        DDLogError("Error");
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
